<%--
  Created by IntelliJ IDEA.
  User: dipu
  Date: 7/20/2018
  Time: 9:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <title>Customer</title>
</head>
<body class="container">
<div class="pull-right">
  <p>
    <a href="/add" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add</a>
  </p>
</div>

<table class="table table-striped">
  <thead>
  <th>Id</th>
  <th>First Name</th>
  <th>Last Name</th>
  <th>Age</th>
  <th>Action</th>
  </thead>
  <c:forEach items="${customers}" var="c">
    <tr>
      <td>${c.id}</td>
      <td>${c.firstName}</td>
      <td>${c.lastName}</td>
      <td>${c.age}</td>
      <td>
        <a href="update?id=${c.id}" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> </a>
        <a href="delete?id=${c.id}" class="btn btn-danger" onclick="return confirm('Are you Sure?')"><span class="glyphicon glyphicon-trash"></span> </a>
      </td>
    </tr>
  </c:forEach>
</table>
</body>
</html>
