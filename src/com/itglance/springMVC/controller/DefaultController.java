package com.itglance.springMVC.controller;

import com.itglance.springMVC.dao.CustomerDAO;
import com.itglance.springMVC.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.SQLException;

@Controller

public class DefaultController {

    @Autowired
    private CustomerDAO customerDAO;

    @RequestMapping(value="/",method=RequestMethod.GET)
    public String index(Model model){
        try {
            model.addAttribute("customers", customerDAO.getAll());
        }catch (Exception e) {

        }
        return "index";
    }

    @RequestMapping(value="/add",method=RequestMethod.GET)
    public String insert(){
        return "add";
    }

    @RequestMapping(value="/save",method=RequestMethod.POST)
    public String insert(@ModelAttribute("customer")Customer customer,Model model) throws SQLException, ClassNotFoundException {
        customerDAO.insert(customer);
        model.addAttribute("customers", customerDAO.getAll());
        return "index";
    }

    @RequestMapping(value="/update",method=RequestMethod.GET)
    public String update(int id,Model model){
        try {
            model.addAttribute(customerDAO.getById(id));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "update";
    }

    @RequestMapping(value="/update",method=RequestMethod.POST)
    public String update(@ModelAttribute("customer")Customer customer,Model model){

        System.out.println(customer.getAge());
        try{
            customerDAO.update(customer);
            model.addAttribute("customers", customerDAO.getAll());
        }catch (Exception e){}
        return "index";
    }

    @RequestMapping(value="/delete",method=RequestMethod.GET)
    public String delete(int id,Model model){
        try {
            customerDAO.delete(id);
            model.addAttribute("customers", customerDAO.getAll());
        }catch(SQLException e){}
        catch (ClassNotFoundException e){}
        return "index";
    }



}
