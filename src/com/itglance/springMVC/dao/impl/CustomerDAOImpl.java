package com.itglance.springMVC.dao.impl;


import com.itglance.springMVC.dao.CustomerDAO;
import com.itglance.springMVC.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository(value = "customerDAO")
public class CustomerDAOImpl implements CustomerDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Customer> getAll(){
        String sql = "SELECT * FROM customer";

        List<Customer> customerList = jdbcTemplate.query(sql, new RowMapper<Customer>() {
            @Override
            public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
                Customer customer = new Customer();
                customer.setId(resultSet.getInt("id"));
                customer.setFirstName(resultSet.getString("first_name"));
                customer.setLastName(resultSet.getString("last_name"));
                customer.setAge(resultSet.getInt("age"));
                customer.toString();
                return customer;

            }
        });
        return customerList;
    }

    public int insert(Customer customer) {

        String sql = "INSERT INTO customer(first_name,last_name,age) VALUES(?,?,?)";
        return jdbcTemplate.update(sql,customer.getFirstName(),customer.getLastName(),customer.getAge());

    }

    public Customer getById(int id){
        String sql = "SELECT * FROM customer WHERE id=" +id+"";
        return jdbcTemplate.query(sql, new ResultSetExtractor<Customer>() {

            @Override
            public Customer extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                if (rs.next()) {
                    Customer customer = new Customer();
                    customer.setId(rs.getInt("id"));
                    customer.setFirstName(rs.getString("first_name"));
                    customer.setLastName(rs.getString("last_name"));
                    customer.setAge(rs.getInt("age"));
                    return customer;
                }

                return null;
            }

        });
    }

    public int update(Customer customer){
//        System.out.println(customer.getFirstName());
        String sql="UPDATE customer SET first_name=?,last_name=?,age=? where id=?";
        return jdbcTemplate.update(sql,customer.getFirstName(),customer.getLastName(),customer.getAge(),customer.getId());
    }

    public int delete (int id){
        String sql = " DELETE FROM customer WHERE id=?";
        return jdbcTemplate.update(sql,id);
    }
}

